filename = 'testlondon.inp'
delim = '='
with open(f'{filename}', 'r') as in_file:
    lines = in_file.readlines()
    for line in lines:
        if '{' in line:
            key = line.split('{')[0].strip()
            print(f'"{key}":{{')
        elif delim in line:
            key, val = line.split(delim)
            print(f'"{key.strip()}":"{val.strip()},"')
        elif '}' in line:
            print('},')
