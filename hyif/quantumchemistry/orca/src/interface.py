from __future__ import annotations

import re
import warnings
from contextlib import suppress
from pathlib import Path
from typing import Any, Callable, Dict, List, Optional, Tuple, Union

import numpy as np
import packaging.version as pv
from hyset import ComputeSettings, create_compute_settings

from ....utils import bohr2angstrom, unique_filename
from ...abc import HylleraasQMInterface, Parser, Runner
from ...molecule import Molecule
from ...presets import SETTINGS_LEGACY as SETTINGS
from .input_parser import OrcaInput
from .output_parser import OrcaOutput
from .parser import Parser as AllParser

QCMETHODS: dict = {'rimp2': 'ri-mp2'}
PROPERTIES: dict = {
    'geometry_optimization': 'opt',
    'analytical_gradient': 'engrad',
    'numerical_gradient': 'numgrad',
    'numerical_hessian': 'numfreq',
    'normal_modes': 'freq',
    'analytical_hessian': 'freq',
    'transition_state': 'OptTS',
}

VERSIONS_SUPPORTED = ['5.0.3']
# move oyther because it can be reused by other programs


class Orca(HylleraasQMInterface):
    """Orca interface."""

    def __init__(self,
                 method: dict,
                 compute_settings: Optional[ComputeSettings] = None):

        self.method = method

        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        self.runner: Runner = self._set_runner(self.compute_settings)
        self.parser: AllParser = self._set_parser(self.compute_settings)
        self.OutputParser = self.parser.OutputParser
        self.InputParser = self.parser.InputParser

        self.version: pv.Version = (self.check_version() if self.method.get(
            'check_version', True) else None)

        self.main_input, self.specific_input = self.get_method(self.method)

    def _set_parallel_input(self, compute_settings: ComputeSettings) -> dict:
        """Set variables for parallel computing.

        Parameter
        ---------
        compute_settings: ComputeSettings
            object containing informations about launcher, mpi_procs
            and smp_threads
        """
        parallel_settings: dict = {}
        num_procs = compute_settings.mpi_procs
        memory = compute_settings.memory

        if num_procs is not None:
            if num_procs >= 1:
                parallel_settings.update(
                    {'PAL': {
                        'NPROCS ': str(compute_settings.mpi_procs)
                    }})
        if memory is not None:
            if memory > 1:
                parallel_settings.update({'MaxCore': str(memory)})
        return parallel_settings

    def _set_runner(self, compute_settings: ComputeSettings) -> Runner:
        """Set runner.

        Parameter
        ---------
        compute_settings: :obj:`ComputeSettings`
            current compute settings

        Returns
        -------
        :obj:`Runner`
            runner for performing calculations

        """
        return getattr(
            self.compute_settings,
            str(SETTINGS[self.compute_settings.name]['runner']),
        )

    def _set_parser(self, compute_settings: ComputeSettings) -> AllParser:
        """Set parsers for input, output, errors.

        Parameter
        ---------
        compute_settings: :obj:`ComputeSettings`
            If the ComputeSettings instance contains an 'options' dict
            with keys 'InputParser', 'OutputParser', the parsers will be
            taken from there.

        Returns
        -------
        :obj:`Parser`

        """
        return AllParser(InputParser=OrcaInput,  # type: ignore
                         OutputParser=OrcaOutput)  # type: ignore

    def abs_path(self, filename: Union[str, Path]) -> Path:
        """Get absolute path of filename in workdir.

        Parameter
        ---------
        filename : str or :obj:`pathlib.Path`
            (relative) path to file

        Returns
        -------
        :obj:`pathlib.Path`
            absolute path for file (assumed to be inside work_dir)

        """
        return Path(self.compute_settings.work_dir) / Path(filename)

    # @contextmanager
    # def _run_ctx(self, input_dict: dict) -> Generator[str, None, None]:
    #     """Run calcultion in context.

    #     Parameter
    #     ---------
    #     input_dict : dict
    #         input

    #     Returns
    #     -------
    #     Generator
    #         generates context

    #     """
    #     output = str(self.runner.run(input_dict))
    #     try:
    #         yield output
    #     finally:
    #         return None

    def check_version(self) -> pv.Version:
        """Check if program version is compatible with interface.

        Returns
        -------
        :obj:`packaging.Version'
            version object

        """
        version: pv.Version = None
        if self.compute_settings.name == 'local':
            v: str = ''
            if hasattr(self.runner, 'run_ctx'):
                with suppress(FileNotFoundError):
                    with self.runner.run_ctx(  # type: ignore
                            {
                            'program': 'orca',
                            'args': ['gibberish']
                            }) as v:
                        version = pv.parse(
                            str(v).split('Program Version')[1].split()[0])
            else:
                input_str = '!UHF STO-3G\n* xyz 0 2\n H 0 0 0\n*\n'
                filename = self.abs_path(
                    unique_filename(input_str.split('\n')) + '.inp')
                with open(filename, 'wt') as f:
                    f.write(input_str)
                output = self.runner.run({
                    'program': 'orca',
                    'args': [filename]
                })
                result = self.OutputParser.parse(str(output))
                np.testing.assert_allclose(result['energy'],
                                           -0.466581849556,
                                           atol=1e-6)

                for file in self.compute_settings.work_dir.iterdir():
                    if file.stem == filename.stem:
                        file.unlink()

                version = pv.parse(result['version'])

        elif self.compute_settings.name == 'saga':
            modules: List[str] = self.compute_settings.modules
            for module in modules:
                _module = module.lower()
                if 'orca' in _module:
                    version = re.findall(r'\d+\.\d+\.\d+', _module)
                    if version:
                        version = pv.Version(version[0])
                    break

        if version not in (pv.parse(v) for v in VERSIONS_SUPPORTED):
            raise NotImplementedError(
                f' Orca version {version} not supported.',
                f' Please contact {self.author} ',
            )

        return version

    @property
    def author(self):
        """Return author's email adress."""
        return 'tilmann.bodenstein@kjemi.uio.no'

    def _translate_kw(self, kwlist: Union[list, str], mapping: dict) -> list:
        """Translate keywords for orca.

        Parameter
        ---------
        kwlist: str or list
            keywords

        Returns
        -------
        list
            list of orca keywords

        """
        if not kwlist:
            return []
        kwlist = [kwlist] if not isinstance(kwlist, list) else kwlist
        return [mapping.get(str(x).lower(), str(x)) for x in kwlist]
        #  list(map(lambda x: mapping.get(str(x).lower(), str(x)), kwlist))

    def get_method(self, method: dict) -> Tuple[list, dict]:
        """Get method information in orca format.

        Parameter
        ---------
        method : dict
            input dictionary as come from hylleraas.Method

        Returns
        -------
        Tuple(list, dict)
            main_input (starts with '!') and specific input (starts with '%')

        """
        main_input: list = []
        specific_input: dict = {}
        input_file = method.get('input_file', None)
        if input_file is not None:
            (
                main_input,
                specific_input,
            ) = self.InputParser.read_method_from_file(  # type: ignore
                self.abs_path(input_file))

        main_input, _, _ = self.add_kw(
            method.get('main_input', None),
            main_input=main_input,
            specific_input={},
        )
        _, specific_input, _ = self.add_kw(
            method.get('specific_input', None),
            main_input=[],
            specific_input=specific_input,
        )

        qcmethod = self._translate_kw(method.get('qcmethod', None), QCMETHODS)
        main_input, _, _ = self.add_kw(qcmethod,
                                       main_input=main_input,
                                       specific_input={})
        basis = str(method.get('basis', '')).lower()
        main_input, _, _ = self.add_kw(basis,
                                       main_input=main_input,
                                       specific_input={})
        props = self._translate_kw(method.get('properties', None), PROPERTIES)
        main_input, _, _ = self.add_kw(props,
                                       main_input=main_input,
                                       specific_input={})

        _, specific_input, _ = self.add_kw(
            {'constrain': method.get('constrain', None)},
            main_input=[],
            specific_input=specific_input,
        )

        return main_input, specific_input

    @classmethod
    def get_input_molecule(cls,
                           input_file: Union[str, Path],
                           parser: Parser = None) -> Molecule:
        """Get molecule from input file.

        Parameters
        ----------
        input_file: :obj:`pathlib.Path`
            path to input file
        parser: :obj:`Parser`, optional
            parser for reading inputfile (must provide method 'parse')

        Returns
        -------
        :obj:`Molecule`
            Hylleraas compatible Molecule object

        """
        parse: Callable
        if parser is None:
            parse = OrcaInput.read_molecule_from_file  # type: ignore
        else:
            parse = parser.parse

        return parse(Path(input_file))  # type: ignore

    @classmethod
    def get_input_method(cls,
                         input_file: Path,
                         parser: Parser = None) -> Tuple[List, Dict]:
        """Get orca input objects from input file.

        Parameters
        ----------
        input_file: :obj:`pathlib.Path`
            path to input file
        parser: :obj:`Parser`, optional
            parser for reading inputfile (must provide method 'parse')

        Returns
        -------
        Tuple(list, dict)
            main_input (starting with '!') and specific input
            (starting with '%')

        """
        parse: Callable
        if parser is None:
            parse = OrcaInput.read_method_from_file  # type: ignore
        else:
            parse = parser.parse

        return parse(Path(input_file))  # type: ignore

    def add_kw(
        self,
        kw: Any,
        main_input: Optional[list] = None,
        specific_input: Optional[dict] = None,
    ) -> Tuple[list, dict, bool]:
        """Add keyword to orca input objects.

        Parameters
        ----------
        kw : Any
            keyword
        main_input: list
            optional
        specific_input : dict
            optional

        Returns
        -------
        Tuple[list, dict, bool]
            main_input, specific_input and boolean if adding was successfull

        """
        if main_input is None:
            main_input = self.main_input.copy()
        if specific_input is None:
            specific_input = self.specific_input.copy()

        added = False
        if not kw:
            return main_input, specific_input, added

        if isinstance(kw, str):
            if kw.lower() not in main_input or kw not in main_input:
                main_input.append(kw.lower())
                added = True
        elif isinstance(kw, list):
            for k in kw:
                if k.lower() not in main_input or k not in main_input:
                    main_input.append(k)
                    added = True
        elif isinstance(kw, dict):
            if 'constrain' in kw.keys():
                if kw['constrain'] is not None:
                    specific_input['geom'] = self._gen_constrain_list(
                        kw['constrain'])
                    added = True
            if 'properties' in kw.keys():
                prop = kw['properties']
                if prop in PROPERTIES:
                    if prop not in main_input:
                        main_input.append(PROPERTIES[prop])
                        added = True
                else:
                    raise Exception(f'property {prop} not supported ',
                                    'in Orca interface')

            kw_clean = kw.copy()
            if 'constrain' in kw_clean:
                del kw_clean['constrain']
            if 'properties' in kw_clean:
                del kw_clean['properties']

            specific_input.update(kw_clean)
            added = True

        return main_input, specific_input, added

    # def rm_kw(self,
    #           kw: Any,
    #           main_input: Optional[list] = None,
    #           specific_input: Optional[dict] = None
    #           ) -> Tuple[list, dict, bool]:
    #     """Remove keyword from orca input objects.

    #     Parameters
    #     ----------
    #     kw : Any
    #         keyword
    #     main_input: list
    #         optional
    #     specific_input : dict
    #         optional

    #     Returns
    #     -------
    #     Tuple[list, dict]
    #         main_input and specific_input

    #     """
    #     if main_input is None:
    #         main_input = self.main_input.copy()
    #     if specific_input is None:
    #         specific_input = self.specific_input.copy()
    #     removed = False
    #     if isinstance(kw, str):
    #         if kw.lower() in main_input or kw in main_input:
    #             main_input.remove(kw.lower())
    #             removed = True
    #         else:
    #             removed = False
    #     elif isinstance(kw, dict):
    #         if 'constrain' in kw.keys():
    #             warnings.warn(
    #                 f'entire block {specific_input["geom"]} '
    #                 'removed from specific_input')
    #             del specific_input['geom']
    #             removed = True

    #         if 'properties' in kw.keys():
    #             prop = kw['properties'].lower()
    #             if prop in PROPERTIES:
    #                 if prop in main_input:
    #                     main_input.remove(PROPERTIES[prop])
    #                     removed = True
    #                 else:
    #                     removed = False
    #             else:
    #                 raise Exception(
    #                     f'could not find kw {kw["properties"].lower()}',
    #                     'in {PROPERTIES}')

    #     return main_input, specific_input, removed

    def _gen_constrain_list(self, constr: Any) -> list:
        """Generate constrain input for orca.

        Parameters
        ----------
        constr: input string or list or list of lists
            constrain input

        Returns
        -------
        list
            corresponding list entry for orca specific input

        """
        constr = [constr] if not isinstance(constr, list) else constr
        constr = [constr] if not isinstance(constr[0], list) else constr

        if len(constr) == 0:
            return []
        const_list = ['  Constraints']
        for c in constr:
            if not isinstance(c, list):
                raise Exception("usage : ['dihedral', 1, 2, 0, 4, 120.] or "
                                "['cartesian', 4], etc. ")
            if c[0] == 'distance':
                cstr = '  { B ' + f'{c[1]} ' + f'{c[2]} ' + f'{c[3]} ' + ' C }'
                const_list.append(cstr)
            if c[0] == 'angle':
                cstr = ('  { A ' + f'{c[1]} ' + f'{c[2]} ' + f'{c[3]} ' +
                        f'{c[4]} ' + ' C }')
                const_list.append(cstr)
            if c[0] == 'dihedrial':
                cstr = ('  { D ' + f'{c[1]} ' + f'{c[2]} ' + f'{c[3]} ' +
                        f'{c[4]} ' + f'{c[5]} ' + ' C }')
                const_list.append(cstr)
            if c[0] == 'cartesian':
                cstr = '  { C ' + f'{c[1] }' + ' C }'
                const_list.append(cstr)
        const_list.append('  end')
        return const_list

    def _set_coord_unit(self,
                        molecule: Molecule,
                        main_input: Optional[list] = None) -> list:
        """Set unit for molecule coordinates.

        Parameter
        ---------
        molecule : :obj:`Molecule`
            Molecule (hylleraas compatible)
        main_input : list, optional
            main_input, defaults to self.main_input

        Returns
        -------
        list
            updated main_input

        """
        if main_input is None:
            main_input = self.main_input.copy()
        mapping = {'bohr': 'bohrs', 'angstrom': 'angs'}

        try:
            unit = molecule.properties.get('unit', 'bohr')
        except KeyError:
            unit = 'bohr'

        main_input = [m for m in main_input if m not in ('bohrs', 'angs')]
        main_input, _, _ = self.add_kw(mapping[unit], main_input=main_input)

        return main_input

    def run(self, molecule: Molecule, options: Optional[dict] = {}) -> dict:
        """Run an orca calculation.

        Parameter
        ---------
        molecule : Molecule
            (hylleraas-compatible) molecule instance
        options : dict
            dictionary with additional options, e.g.
                options={'constrain': ['angle', 1, 0, 2, 106]}
            or
                options={'constrain': [['distance', 1, 0, 5.0],
                         ['cartesian', 5] ],
                         'specific_input' = {'SCF':{'maxiter': 500}}
            or
                options = { 'main_input': 'D3' }'
            etc.

        Returns
        -------
        dict
            parsed results

        """
        main_input, _, _ = self.add_kw(
            options.get('main_input', None),
            main_input=None,
            specific_input={},
        )
        _, specific_input, _ = self.add_kw(
            options.get('specific_input', None),
            main_input=[],
            specific_input=None,
        )
        main_input = self._set_coord_unit(molecule, main_input)

        specific_input.update(self._set_parallel_input(self.compute_settings))
        input_str = self.InputParser.gen_orca_input(  # type: ignore
                                    main_input, specific_input, molecule)

        filename = unique_filename(input_str.split('\n'))
        infile = self.abs_path(filename + '.inp')
        outfile = self.abs_path(filename + '.out')

        with open(infile, 'wt') as f:
            f.write(input_str)

        orca_ex = self.compute_settings.find_bin_in_env('orca')
        # orca requires full path when running in parallel
        running_dict = {
            'program': orca_ex,
            'args': [str(infile)],
            'output_file': outfile,
        }

        self.runner.run(running_dict)

        result = self.OutputParser.parse(outfile)

        result['input'] = input_str
        result['input_file'] = str(infile)
        result['output_file'] = str(outfile)

        if result['warnings']:
            warnings.warn('\n'.join(result['warnings']))
        if result['errors']:
            raise RuntimeError('\n'.join(result['errors']))

        return result

    def get_energy(self, molecule: Molecule) -> float:
        """Compute energy with orca.

        Paramters
        ---------
        molecule : :obj:`Molecule` (hylleraas-compatible)
            molecule for which energy should be computed

        Returns
        -------
        float
            energy

        """
        result = self.run(molecule)
        return result['energy']

    def get_gradient(self,
                     molecule: Molecule,
                     gradient_type: Optional[str] = None) -> np.ndarray:
        """Compute gradient with orca.

        Paramters
        ---------
        molecule : :obj:`Molecule` (hylleraas-compatible)
            molecule for which energy should be computed
        gradient_type: str, optional
            gradient type ('analytical' or 'numerical')


        Returns
        -------
        np.ndarray
            gradient in unit Hartee/(Molecule.properties['unit'])

        """
        kw = self._set_deriv_type('gradient', gradient_type)
        result = self.run(molecule, options={'main_input': [kw]})
        # gradient=result['gradient']
        # below has more digits
        gradient = self.OutputParser.extract_gradient(  # type: ignore
            Path(result['output_file']))
        if 'angs' in self.main_input:
            gradient = gradient / bohr2angstrom
        return gradient

    def _set_deriv_type(self, derivative: str, input_type: str) -> str:
        """Set type of derivative.

        Parameter
        ---------
        derivative: str
            'gradient' or 'hessian'
        input_type : str
            'analytical' or 'numerical' or None.
            if None, taken from method input, if that is also
            None, choose analytical.

        Returns
        -------
        str
            derivative type

        """
        mapping = {
            'analytical': PROPERTIES['analytical_' + derivative],
            'numerical': PROPERTIES['numerical_' + derivative],
        }
        if input_type is not None:
            return mapping[input_type]
        else:
            return mapping[self.method.get(derivative + '_type', 'analytical')]

    def get_hessian(self,
                    molecule,
                    hessian_type: Optional[str] = None) -> np.ndarray:
        """Compute hessian with orca.

        Paramters
        ---------
        molecule : :obj:`Molecule` (hylleraas-compatible)
            molecule for which energy should be computed
        hessian_type: str, optional
            hessian type ('analytical' or 'numerical')

        Returns
        -------
        np.ndarray
            hessian in unit Hartee/(Molecule.properties['unit'])**2

        """
        kw = self._set_deriv_type('hessian', hessian_type)
        result = self.run(molecule, options={'main_input': [kw]})
        hessian = self.OutputParser.extract_hessian(Path(  # type: ignore
            result['output_file']))
        if 'angs' in self.main_input:
            hessian = hessian / bohr2angstrom**2
        return hessian

    def get_normal_modes(self,
                         molecule,
                         hessian_type: Optional[str] = None) -> dict:
        """Compute normal modes with orca.

        Paramters
        ---------
        molecule : :obj:`Molecule` (hylleraas-compatible)
            molecule for which energy should be computed
        hessian_type: str, optional
            hessian type ('analytical' or 'numerical')

        Returns
        -------
        dict
            frequencies (with minus sign indicating negative force constants,
            a.k.a. 'imaginary frequencies'), normal modes and hessian

        """
        kw = self._set_deriv_type('hessian', hessian_type)
        result = self.run(molecule, options={'main_input': [kw]})
        output_path = Path(result['output_file'])
        hessian = self.OutputParser.extract_hessian(  # type: ignore
            output_path)
        frequencies = (self.OutputParser.
                       extract_vibrational_frequencies)(  # type: ignore
            output_path)
        normal_modes = self.OutputParser.extract_normal_modes(  # type: ignore
            output_path)

        return {
            'hessian': hessian,
            'frequencies': frequencies,
            'normal_modes': normal_modes,
        }
