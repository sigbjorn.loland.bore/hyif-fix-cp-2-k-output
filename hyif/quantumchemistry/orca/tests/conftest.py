import os

import pytest

FIXTURE_DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    'data',
)


def pytest_configure():
    """Set global pytest variables."""
    pytest.FIXTURE_DIR = FIXTURE_DIR
