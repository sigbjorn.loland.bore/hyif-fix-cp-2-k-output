from abc import ABC, abstractmethod
from pathlib import Path
from typing import Any, Union

import numpy as np


class HylleraasQMInterface(ABC):
    """Base class for quantum chemistry Interfaces."""

    @property
    @abstractmethod
    def check_version(self) -> str:
        """Check the version."""
        pass  # pragma no cover

    @property
    @abstractmethod
    def author(self) -> str:
        """Set the authors email adress."""
        pass  # pragma no cover

    @property
    @abstractmethod
    def get_energy(self, molecule: Any) -> float:  # type: ignore
        """Compute Energy."""
        pass  # pragma no cover

    @property
    @abstractmethod
    def get_gradient(self, molecule: Any) -> np.array:  # type: ignore
        """Compute Gradient."""
        pass  # pragma no cover

    @property
    @abstractmethod
    def get_hessian(self, molecule: Any) -> np.array:  # type: ignore
        """Compute Hessian."""
        pass  # pragma no cover


class Runner(ABC):
    """Runner base class."""

    @abstractmethod
    def run(self, input_dict: dict) -> Union[str, dict]:
        """Run a calculation."""
        pass


class Parser(ABC):
    """Parser base class."""

    @abstractmethod
    def parse(self, input: Union[str, Path]) -> dict:
        """Parse file or input_str."""
        pass
