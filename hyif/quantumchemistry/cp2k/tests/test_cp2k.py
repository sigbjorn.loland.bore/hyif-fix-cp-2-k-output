import numpy as np
# import pytest
from hyobj import Molecule
from hyset import create_compute_settings

from hyif import CP2K  # type: ignore


def test_input():
    """Test cp2k input."""
    mycp2k = CP2K({
        'options': {
            'FORCE_EVAL.DFT.SCF.Max_scf': 300,
            'FORCE_EVAL.Method': 'Quickstep',
        }
    })

    assert hasattr(mycp2k, 'template')


def test_parallel():
    """Test parallel input."""
    myenv = create_compute_settings('local', mpi_procs=2, smp_threads=4)
    mycp2k = CP2K({}, compute_settings=myenv)
    assert mycp2k.launcher == 'mpirun -n 2'
    assert mycp2k.executable == 'cp2k.psmp'

    myenv = create_compute_settings('local', smp_threads=6)
    mycp2k = CP2K({}, compute_settings=myenv)
    assert mycp2k.launcher == []
    assert mycp2k.executable == 'cp2k.ssmp'

    myenv = create_compute_settings('local')
    mycp2k = CP2K({}, compute_settings=myenv)
    assert mycp2k.launcher == []
    assert mycp2k.executable == 'cp2k.sopt'


def test_integration():
    """Test actual cp2k calculation."""
    mol_str = """ O   0.0000   0.0000   0.0626
 H  -0.7920   0.0000  -0.4973
 H   0.7920   0.0000  -0.4973"""
    mymol1 = Molecule(mol_str, properties={'unit': 'angstrom'})
    mymol2 = Molecule(
        {
            'atoms': mymol1.atoms,
            'coordinates': mymol1.coordinates / 0.529177249,
        },
        properties={'unit': 'bohr'},
    )

    myenv = create_compute_settings('local')
    mycp2k = CP2K(
        {
            'options': {
                'FORCE_EVAL.DFT.SCF.Max_scf': 20,
                'FORCE_EVAL.Method': 'Quickstep',
            }
        },
        compute_settings=myenv,
    )

    out1 = mycp2k.run(mymol1)
    en2 = mycp2k.get_energy(mymol2)
    np.testing.assert_almost_equal(out1['energy'], en2)
