import warnings
from contextlib import suppress
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Optional, Tuple, Union

import numpy as np
import packaging.version as pv

from ...molecule import Molecule

try:
    import pycp2k

    found_pycp2k = True
except ImportError:
    found_pycp2k = False

from hyset import ComputeSettings, create_compute_settings

from ....utils import unique_filename
from ...abc import HylleraasQMInterface, Parser, Runner
from ...presets import SETTINGS_LEGACY as SETTINGS
from .output_parser import OutputParser
from .template import TEMPLATE

VERSIONS_SUPPORTED = ['7.1', '2022.2']


class CP2K(HylleraasQMInterface):
    """CP2K interface."""

    def __init__(self,
                 method: dict,
                 compute_settings: Optional[ComputeSettings] = None):

        if not found_pycp2k:
            raise ImportError('could not find package pycp2k')

        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        self.executable: str
        self.launcher: Union[list, str]
        self.executable, self.launcher = self._set_executable(
            method, self.compute_settings)

        self.template = self.get_template(method)

        self.options = method.get('options', {})

        self.runner: Runner = getattr(
            self.compute_settings,
            str(SETTINGS[self.compute_settings.name]['runner']),
        )

        self.parser: Parser = self._set_parser(self.compute_settings)

        to_check = method.get('check_version', True)
        self.version: pv.Version = self.check_version() if to_check else None

        self.calc = pycp2k.CP2K()
        self.calc.parse(self.template)

        CP2K_INPUT = self.calc.CP2K_INPUT  # noqa N806
        GLOBAL = CP2K_INPUT.GLOBAL  # noqa N806
        FORCE_EVAL = CP2K_INPUT.FORCE_EVAL_list[0]  # noqa N806
        SUBSYS = FORCE_EVAL.SUBSYS  # noqa N806
        DFT = FORCE_EVAL.DFT  # noqa N806
        SCF = DFT.SCF  # noqa N806

        self.subsys = SUBSYS

        if self.options:
            for k, v in self.options.items():
                if self.is_int(v) or isinstance(v, int):
                    exec('%s = %d' % (k, int(v)))
                elif self.is_float(v) or isinstance(v, float):
                    exec('%s = %20.14f' % (k, float(v)))
                else:
                    exec("%s = '%s'" % (k, v))

    def _set_parser(self, compute_settings: ComputeSettings) -> Parser:
        """Set parsers for input, output, errors.

        Parameter
        ---------
        compute_settings: :obj:`ComputeSettings`
            If the ComputeSettings instance contains an 'options' dict
            with keys 'InputParser', 'OutputParser', the parsers will be
            taken from there.

        Returns
        -------
        :obj:`Parser`

        """
        return OutputParser  # type: ignore

    def _set_executable(
            self, method: dict,
            compute_settings: ComputeSettings) -> Tuple[str, Union[list, str]]:
        """Set executable for seriel/parallel compuation with CP2K.

        Parameter
        ---------
        method: dict
            method input

        compute_settings: :obj:`hyset.ComputeSettings`
            compute_Settings

        Returns
        -------
        str
            name of binary

        """
        executable: str = method.get('executable', 'cp2k.sopt')
        num_procs = compute_settings.mpi_procs
        smp_threads = compute_settings.smp_threads
        launcher: Union[list, str]
        if num_procs is not None:
            executable = 'cp2k.psmp'
            launcher = f'mpirun -n {num_procs}'
        else:
            if smp_threads is not None:
                executable = 'cp2k.ssmp'
                launcher = []
            else:
                executable = 'cp2k.sopt'
                launcher = []

        return executable, launcher

    def check_version(self) -> pv.Version:
        """Check if program version is compatible with interface.

        Returns
        -------
        :obj:`packaging.Version'
            version object

        """
        version: pv.Version = None

        cp2k_ex = self.compute_settings.find_bin_in_env(self.executable)
        if self.compute_settings.name == 'local':
            v: str = ''
            if hasattr(self.runner, 'run_ctx'):
                with suppress(UserWarning):
                    with self.runner.run_ctx(  # type: ignore
                           {
                            'program': cp2k_ex,
                            'args': ['--version']
                            }) as v:
                        version = pv.parse(
                                    str(v).split('CP2K version')[1].split()[0])

        if version not in (pv.parse(v) for v in VERSIONS_SUPPORTED):
            raise NotImplementedError(
                f' CP2K version {version} not supported.',
                f' Please contact {self.author} ',
            )

        return version

    def get_template(self, method: dict) -> Path:
        """Get template for CP2K from mthod."""
        _input = method.get('input')
        try:
            path = self.abs_path(_input)
        except (OSError, TypeError):
            pass
        else:
            return path

        if isinstance(_input, str):
            filename = self.abs_path(
                unique_filename(_input.split('\n')) + '.inp')
            with open(filename, 'wt') as f:
                f.write(_input)

            return filename

        _template = method.get('template')
        try:
            path = self.abs_path(_template)
        except (OSError, TypeError):
            pass
        else:
            return path

        filename = self.abs_path(
            unique_filename(TEMPLATE.split('\n')) + '.inp')
        with open(filename, 'wt') as f:
            f.write(TEMPLATE)

        return filename

    def abs_path(self, filename: Union[str, Path]) -> Path:
        """Get absolute path of filename in workdir.

        Parameter
        ---------
        filename : str or :obj:`pathlib.Path`
            (relative) path to file

        Returns
        -------
        :obj:`pathlib.Path`
            absolute path for file (assumed to be inside work_dir)

        """
        return Path(self.compute_settings.work_dir) / Path(filename)

    def get_hessian(self, molecule: Any):
        """Compute hessian."""
        pass

    def run(
        self,
        molecule: Molecule,
        unit_cell: Optional[Union[list, np.array]] = None,
        pbc: Optional[list] = None,
    ) -> dict:
        """Run a cp2k calculation.

        Parameter
        ---------
        molecule: :obj:`hyobj.Molecule`
            Molecule
        unit_cell : list or np.ndarray, optional
            unit_cell, default None
        pbc: list, optional
            periodic boundary conditions, default None


        Returns
        -------
        dict
            parsed results

        """
        unit = molecule.properties.get('unit', 'bohr')
        atoms = molecule.atoms
        coords = np.array(molecule.coordinates)

        if 'bohr' in unit.lower():
            self.subsys.COORD.Unit = 'BOHR'
        elif 'angs' in unit.lower():
            self.subsys.COORD.Unit = 'ANGSTROM'
        else:
            raise TypeError(f'unit {unit} unknown')

        ase_mol = [
            self.AseAtom(atoms[i], coords[i]) for i in range(len(atoms))
        ]

        self.calc.create_coord(self.subsys, ase_mol)

        if pbc is not None and unit_cell is not None:
            self.calc.create_cell(self.subsys, self.AseCell(unit_cell, pbc))
        elif pbc is None and unit_cell is not None:
            self.subsys.CELL.A = unit_cell[0]
            self.subsys.CELL.B = unit_cell[1]
            self.subsys.CELL.C = unit_cell[2]

        input_str = self.calc.get_input_string()
        filename = unique_filename(input_str.split('\n'))
        infile = self.abs_path(filename + '.inp')
        outfile = self.abs_path(filename + '.out')
        with open(infile, 'wt') as f:
            f.write(input_str)
        cp2k_ex = self.compute_settings.find_bin_in_env(self.executable)

        running_dict = {
            'program': cp2k_ex,
            'args': [str(infile)],
            'output_file': outfile,
            'launcher': self.launcher,
        }

        _ = self.runner.run(running_dict)

        result: dict = self.parser.parse(outfile)

        result['input_file'] = str(infile)
        result['output_file'] = str(outfile)

        if result['errors']:
            warnings.warn('\n'.join(result['errors']))

        # reset
        self.subsys.COORD.Unit = None
        return result

    def get_gradient(
        self,
        molecule,
        unit_cell: Optional[Union[list, np.array]] = None,
        pbc: Optional[list] = None,
    ):
        """Compute gradient."""
        result = self.run(molecule, unit_cell, pbc)
        return result['gradient']

    def get_energy(
        self,
        molecule,
        unit_cell: Optional[Union[list, np.array]] = None,
        pbc: Optional[list] = None,
    ):
        """Compute energy."""
        result = self.run(molecule, unit_cell, pbc)
        return result['energy']

    def is_float(self, instr):
        """Check if string is float."""
        try:
            float(instr)
        except Exception:
            return False
        else:
            return True

    def is_int(self, instr):
        """Check if string is integer."""
        if isinstance(instr, int):
            return True
        return instr.isdigit()

    @dataclass
    class AseAtom:
        """Conversion."""

        symbol: str
        position: Union[list, np.array]

    class AseCell:
        """Required by pycp2k."""

        def __init__(self, cell, pbc):
            self.cell = cell
            self.pbc = pbc

        def get_cell(self):
            """Return unit cell."""
            return self.cell

        def get_pbc(self):
            """Return periodic boundary conditions."""
            return self.pbc

    @property
    def author(self):
        """Return who wrote this interface."""
        return 'tilmannb@uio.no'
