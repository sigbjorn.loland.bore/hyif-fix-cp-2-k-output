import json
import os
import shutil
from typing import Optional

from hyset import ComputeSettings
from qcelemental import PhysicalConstantsContext

from ...importme import HylleraasInterface
from ...utils import get_val_from_arglist, key_in_arglist
from .parser import MRChemInput, MRChemOutput

constants = PhysicalConstantsContext('CODATA2018')


class MRChem(HylleraasInterface):
    """MRChem interface."""

    def __init__(self, method,
                 compute_settings: Optional[ComputeSettings] = None):
        # TODO use compute_settings
        self.method = method
        if key_in_arglist(self.method, look_for='executable'):
            self.executable = get_val_from_arglist(self.method,
                                                   look_for='executable')
        else:
            self.executable = 'mrchem'

        if key_in_arglist(self.method, look_for='filename'):
            self.filename = get_val_from_arglist(self.method,
                                                 look_for='filename')
        else:
            self.filename = 'mymrchemcalc'

        # Input file in JSON format, but with suffix '.inp'
        self.filename_inp = self.filename + str('.inp')
        self.filename_out = self.filename + str('.json')

    @classmethod
    def get_input_method(cls, *args) -> dict:
        """Get method input."""
        method: dict = {}
        for arg in args:
            if isinstance(arg, str):
                if os.path.isfile(arg):
                    method = MRChemInput.mrchem2json(arg)

        return method

    @classmethod
    def get_input_molecule(cls, *args) -> dict:
        """Get molecule input."""
        molecule: dict = {}
        for arg in args:
            if isinstance(arg, str):
                if os.path.isfile(arg):
                    molecule = MRChemInput.mrchem2mol(arg)

        return molecule

    def get_energy(self, molecule):
        """Compute energy with MRChem.

        Returns
        -------
        :obj:`float`
            energy

        """
        if shutil.which(self.executable) is None:
            raise Exception(f'{self.executable} not found in PATH')
        input_dict = MRChemInput.gen_mrchem_input(self.method, molecule)
        with open(self.filename_inp, 'w') as input_file:
            json.dump(input_dict, input_file)
        # os.system(f'{self.executable}
        #     --executable={self.executable}.x --json {self.filename}')
        energy = MRChemOutput.mrchem_output_parser_energy(self.filename_out)
        return energy

    def get_gradient(self, molecule):
        """Parse gradient from Energy computation with MRChem.

        Returns
        -------
        :obj:`array`
            gradient

        """
        if not os.path.isfile(self.filename_out):
            if shutil.which(self.executable) is None:
                raise Exception(f'{self.executable} not found in PATH')
            input_dict = MRChemInput.gen_mrchem_input(self.method, molecule)
            with open(self.filename_inp, 'w') as input_file:
                json.dump(input_dict, input_file)
            # os.system(f'{self.executable}
            #     --executable={self.executable}.x --json {self.filename}'
            #           )

        gradient = MRChemOutput.mrchem_output_parser_gradient(
            self.filename_out, molecule.num_atoms).ravel()

        return gradient

    @property
    def version(self):
        """Set interface version."""
        return '0.0'

    @property
    def author(self):
        """Set author's name or email adress."""
        return 'bin.gao@uit.no'
