import inspect
import json

import numpy as np
from qcelemental import PhysicalConstantsContext

constants = PhysicalConstantsContext('CODATA2018')


class MRChemInput:
    """Read input from MRChem input File."""

    def __init__(self):
        pass

    @classmethod
    def mrchem2mol(cls, filename: str) -> dict:
        """Read Molecule from MRChem input file."""
        # MRChem has molecule information in,
        # "Molecule": {
        #   "coords": "F -1.194868474027 -0.244625419775 0.340936636348\n
        # H -1.672505086978 -0.395082674713 1.107306807211\n"
        # }
        with open(f'{filename}', 'r') as input_file:
            data = json.load(input_file)

        input_mol = data['Molecule']['coords'].split()
        atoms = []
        coordinates = []
        for i in range(0, len(input_mol), 4):
            atoms.append(input_mol[i])
            # FIXME: np.float32 loses precision?
            coordinates.append(np.array(input_mol[i + 1:i + 4],
                               dtype=np.float64))
        return {'atoms': atoms, 'coordinates': coordinates}

    @classmethod
    def mrchem2json(cls, filename: str) -> dict:
        """Read MRChem import quick-and-dirty."""
        with open(f'{filename}', 'r') as input_file:
            method_dict = json.load(input_file)
        # Remove molecule information
        method_dict.pop('Molecule', None)
        return method_dict

    @classmethod
    def gen_mrchem_input(cls, method, molecule) -> dict:
        """Generate input for MRChem.

        Parameters
        ----------
        method : dict
            MRChem options dictionary
        molecule : :obj:`Hylleraas.Molecule`
            molecule input

        Returns
        -------
        :obj:`dict`
            input dict

        """
        if isinstance(method, dict):
            _method = method
        elif inspect.isclass(type(method)):
            _method = method.method

        mrchem_input = MRChemInput.set_mrchem_options(_method)

        # FIXME: charge, multiplicity?
        #  maybe this way?
        if 'charge' in molecule.properties:
            charge = molecule.properties['charge']
        else:
            charge = 0

        if 'multiplicity' in molecule.properties:
            multiplicity = molecule.properties['multiplicity']
        else:
            multiplicity = 1

        mrchem_mol_string = ''
        num_atoms = len(molecule.atoms)
        for i in range(0, num_atoms):
            mrchem_mol_string += molecule.atoms[i]
            for j in range(3):
                mrchem_mol_string += ' ' + str(molecule.coordinates[i][j])
            mrchem_mol_string += '\n'

        mrchem_input['Molecule'] = {'multiplicity': multiplicity,
                                    'charge': charge,
                                    'coords': mrchem_mol_string}

        return mrchem_input

    @staticmethod
    def set_mrchem_options(options: dict) -> dict:
        """Set default options for MRChem.

        Parameters
        ----------
        options : :obj:`dict`
            user-defined options

        Returns
        -------
        :obj:`dict`
            full (default + user-defined) options dictionary for MRChem

        """
        # For MRChem, the overall precision 'world_prec' and the method
        # 'WaveFunction.method' must be provided
        mrchem_opt = options
        if 'world_prec' not in mrchem_opt:
            mrchem_opt['world_prec'] = 1.0e-3
        if 'WaveFunction' in mrchem_opt:
            if 'method' not in mrchem_opt['WaveFunction']:
                mrchem_opt['WaveFunction']['method'] = 'LDA'
        else:
            mrchem_opt['WaveFunction'] = {'method': 'LDA'}
        return mrchem_opt


class MRChemOutput:
    """Read from MRChem output file (json format)."""

    def __init__(self):
        pass

    @staticmethod
    def mrchem_output_parser_energy(filename: str) -> float:
        """Extract energy from MRChem output file (json format).

        Returns
        -------
        :obj:`float`
            energy

        """
        with open(f'{filename}', 'r') as output_file:
            data = json.load(output_file)
        return data['output']['properties']['scf_energy']['E_tot']

    @staticmethod
    def mrchem_output_parser_gradient(filename: str,
                                      num_atoms: int) -> np.array:
        """Extract gradient from MRChem output file (json format).

        Returns
        -------
        :obj:`np.array`
            gradient

        """
        scale = constants.bohr2angstroms
        with open(f'{filename}', 'r') as output_file:
            data = json.load(output_file)

        gradient = np.zeros((num_atoms, 3))
        k = 0
        for i in range(num_atoms):
            for j in range(3):
                gradient[i][j] = (data['output']['properties']
                                  ['geometric_derivative']['geom-1']
                                  ['total'][k]) * scale
                k += 1

        return gradient

    @staticmethod
    def total_spin(filename: str) -> float:
        """Extract total spin from MRChem output file (json format)."""
        with open(f'{filename}', 'r') as output_file:
            data = json.load(output_file)
        return 0.5 * (data['output']['properties']['multiplicity'] - 1)
