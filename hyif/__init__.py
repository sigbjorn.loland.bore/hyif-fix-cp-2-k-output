# from .dummy import Dummy, Dummy2
# from .quantumchemistry import (Dalton, CP2K, London, LSDalton, MRChem,
#                                Orca, Xtb)
from .machinelearning import DeepMD
from .quantumchemistry import CP2K, Dalton, MRChem, Orca

PROGRAMS: dict = {
    'dummy': 'Dummy', 'dummy2': 'Dummy2', 'dalton': 'Dalton',
    'london': 'London', 'lsdalton': 'LSDalton', 'mrchem': 'MRChem',
    'orca': 'Orca', 'xtb': 'Xtb', 'cp2k': 'CP2K', 'deepmd': 'DeepMD'
}
