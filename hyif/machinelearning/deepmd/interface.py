import json
from pathlib import Path
from typing import Optional, Union

import numpy as np
import pandas as pd
from hyobj import DataSet
from hyset import ComputeSettings, create_compute_settings
from qcelemental import periodictable as pt

from ...utils import unique_filename, update_nested_dict
from ..abc import Runner


class DeepMD:
    """Interface to DeePMD."""

    def __init__(self,
                 method: dict,
                 compute_settings: Optional[ComputeSettings] = None):

        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        self.runner: Runner = getattr(self.compute_settings, 'Runner')

        self.options = method.get('options', None)

        self.input = method.get('input', None)

        if isinstance(self.input, (str, Path)):
            path = Path(self.input)
            wd_path = Path(self.compute_settings.work_dir / self.input)

            if path.is_absolute():
                with open(wd_path, 'rt') as f:
                    input_dict = json.load(f)
            elif wd_path.exists():
                with open(wd_path, 'rt') as f:
                    input_dict = json.load(f)
            elif path.exists():
                with open(path, 'rt') as f:
                    input_dict = json.load(f)
            else:
                try:
                    input_dict = json.loads(str(self.input))
                except Exception:
                    raise Exception('could not read input file')
        else:
            default_dict = {
                '_comment': "that's all", 'model': {
                    'type_map': [], 'descriptor': {
                        'type': 'se_e2_a', 'rcut_smth': 0.5, 'rcut': 6.0,
                        'neuron': [25, 50, 100], 'resnet_dt': False,
                        'axis_neuron': 16, 'seed': 1, '_comment': " that's all"
                    }, 'fitting_net': {
                        'neuron': [240, 240, 240], 'resnet_dt': True,
                        'seed': 1, '_comment': " that's all"
                    }, '_comment': " that's all"
                }, 'learning_rate': {
                    'type': 'exp', 'decay_steps': 5000, 'start_lr': 0.001,
                    'stop_lr': 3.51e-08, '_comment': "that's all"
                }, 'loss': {
                    'type': 'ener', 'start_pref_e': 0.02, 'limit_pref_e': 1,
                    'start_pref_f': 1000, 'limit_pref_f': 1, 'start_pref_v': 0,
                    'limit_pref_v': 0, '_comment': " that's all"
                }, 'training': {
                    'training_data': {
                        'systems': [], 'batch_size': 'auto',
                        '_comment': "that's all"
                    }, 'numb_steps': 1000000, 'seed': 10,
                    'disp_file': 'lcurve.out', 'disp_freq': 100,
                    'save_freq': 1000, '_comment': "that's all"
                }
            }
            input_dict = update_nested_dict(default_dict,
                                            method,
                                            exclude=['compute_settings'])

        self.input_dict = input_dict

    def version(self):
        """Get version."""
        return 'DeePMD-kit v2.1.5'

    def author(self):
        """Get author of this interface."""
        return 'tilmannb@uio.no'

    def train(self,
              data: Optional[DataSet] = None,
              train_dirs: Optional[list] = None,
              restart=True):
        """Train the model."""
        result: dict = {}
        if not train_dirs:
            train_dirs = self.input_dict['training']['training_data'].get(
                'systems', [])
        for d in train_dirs:
            if not Path(d).exists():
                raise Exception(f'could not find training directory {d}')

        if not train_dirs:
            train_dirs = [self.compute_settings.work_dir]

        self.train_dir = Path(train_dirs[0])

        input_update = {
            'training': {'training_data': {'systems': [str(self.train_dir)]}}
        }
        input_dict = update_nested_dict(self.input_dict, input_update)

        if data is not None:
            type_map = self.prepare_for_deepmd(data, self.train_dir)
            input_update = {'model': {'type_map': type_map}}
            input_dict = update_nested_dict(self.input_dict, input_update)
        # else:
        #     dp1 = ['type.raw', 'type_map.raw']
        #     for d in dp1:
        #         if d not in [f.name for f in list(self.train_dir.iterdir())]:
        #             raise Exception(f'could not find{d} in {self.train_dir}')

        else:
            #  start from former checkpoint
            pass

        self.filename = unique_filename([str(input_dict)])
        path = self.compute_settings.work_dir / str(self.filename + '.json')
        input_file = open(path, 'w')
        json.dump(input_dict, input_file, indent=4)
        input_file.close()

        # try:
        #     np.load('coord.npy')
        # except Exception:
        # np_load_old = np.load
        # np.load = lambda *a,**k: np_load_old(*a, allow_pickle=True, **k)
        args_train = ['train', str(path)]

        if restart and 'checkpoint' in [
                file.name
                for file in list(self.compute_settings.work_dir.iterdir())
        ]:
            args_train = ['train', '-r', 'model.ckpt', str(path)]

        result['training_args'] = args_train

        running_dict = {'program': 'dp', 'args': args_train}
        output = self.runner.run(running_dict)

        result['input_file'] = str(path)
        path = self.compute_settings.work_dir / str(self.filename + '.out')
        result['output_file'] = str(path)
        with open(path, 'wt') as f:
            f.write(str(output))

        args_freeze = ['freeze', '-o', f'graph-{self.filename}.pb']
        result['args_freeze'] = args_freeze

        running_dict = {'program': 'dp', 'args': args_freeze}
        self.runner.run(running_dict)

        args_compress = [
            'compress', '-i', f'graph-{self.filename}.pb', '-o',
            f'graph-{self.filename}-compress.pb'
        ]
        result['args_compress'] = args_compress

        running_dict = {'program': 'dp', 'args': args_compress}
        self.runner.run(running_dict)

        return result

    def run_md(self, graphname=None, program='lammps'):
        """Run md with lammps."""
        if graphname is None:
            graphname = f'graph-{self.filename}.pb'

        input_dict = {'pair_style': f'deepmd {graphname}', 'pair_coff': '* *'}
        input_str = ''
        for k, v in input_dict.items():
            input_str += str(k) + '  ' + str(v) + '\n'
        self.filename_lammps = unique_filename([str(input_dict)])
        path = self.compute_settings.work_dir / \
            str(self.filename_lammps + '.inp')
        with open(path, 'wt') as f:
            f.write(input_str)
        args_lammps = '-in', 'graphname'
        lammps_out = self.runner.run('lmp_serial', *args_lammps)
        return lammps_out

    def prepare_for_deepmd(self, data_in, train_dir: Union[str, Path]):
        """Prepare files for deepMD."""
        path = Path(train_dir) / 'set.000'
        if not path.exists():
            self.compute_settings.create_path(path, create=True)

        data = data_in.data if isinstance(data_in, DataSet) else data_in

        if isinstance(data, pd.DataFrame):

            # perform consistencey check for that!! e.g. as
            #        compare hashes between atoms
            # maybe try to rearrange...
            # try Z instead of 0,1,...,N
            # has to be consistent with lammps

            atoms = data['atoms'][0]
            type_map_raw = []
            type_raw = []

            iatom = 0
            for i, atom in enumerate(atoms):
                if atom in pt.E:
                    type_map_raw.append(atom)
                    atoms[i] = str(iatom)
                    for j in range(i + 1, len(atoms)):
                        atoms[j] = str(iatom) if atoms[j] == atom else atoms[j]
                    iatom += 1

            type_raw = atoms

            file = path.parents[0] / 'type.raw'
            type_str = '\n'.join(type_raw) + '\n'
            with open(file, 'wt') as f:
                f.write(type_str)

            file = path.parents[0] / 'type_map.raw'
            map_str = '\n'.join(type_map_raw) + '\n'
            with open(file, 'wt') as f:
                f.write(map_str)

            cols = list(data.columns)
            dpfiles = {
                'coordinates': 'coord', 'energy': 'energy', 'unit_cell': 'box',
                'gradient': 'force'
            }
            for k, v in dpfiles.items():
                if k not in cols:
                    raise Exception(f'missing column {k} in dataframe ')
                file = path / f'{v}.npy'
                vals = data[k].values
                if vals.dtype == 'object':
                    array = []
                    if len(vals.shape) == 1:
                        for val in vals:
                            array.append(
                                np.array(val, dtype=np.float64).ravel())
                    a = np.array(array)
                else:
                    a = vals
                with open(file, 'w') as f:
                    np.save(f, a, allow_pickle=False)

            return type_map_raw

        else:
            dp_files = ['coord.npy', 'energy.npy', 'box.npy', 'force.npy']
            for d in dp_files:
                if d not in [f.name for f in list(path.iterdir())]:
                    raise Exception(f'could not find file {d} in {path}')

            return None


if __name__ == '__main__':

    from hyobj import DataSet as Ds

    from ..cp2k import CP2K

    ddir = '/Users/tilmann/Documents/work/'
    ddir += 'hylleraas/hyobj/hyobj/dataset/data/sigbj'

    ds1 = Ds(
        ddir,
        options={'match': ['*.inp'], 'parser': CP2K.InputParser.parse})

    ds2 = Ds(
        ddir,
        options={'match': ['*.out'], 'parser': CP2K.OutputParser.parse})
    ds = ds1 + ds2

    # print(ds.data.shape, ds.data.size, ds.get_columns())
    train, test = ds.split(size=[0.8, 0.2], strategy='random')
    ml = DeepMD({'training': {'numb_steps': 100}})
    # ml.train(train, train_dirs=['hyif/deepmd/data_train'], restart=False)
    ml.run_md(graphname='graph-c6f3c14ec5b0bad7b578b3464d5b5d7294c3f1d4.pb')
