from abc import ABC, abstractmethod
from pathlib import Path
from typing import Union


class Runner(ABC):
    """Runner base class."""

    @abstractmethod
    def run(self, input_dict: dict) -> Union[str, dict]:
        """Run a calculation."""
        pass


class Parser(ABC):
    """Parser base class."""

    @abstractmethod
    def parse(self, input: Union[str, Path]) -> dict:
        """Parse file or input_str."""
        pass
