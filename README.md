# Hylleraas Interfaces

## Package description

Collection of Interfaces for Hylleraas Software Platform.

## Howto create own interface

see [here](https://hylleraas.readthedocs.io/en/latest/developers_guide.html)
